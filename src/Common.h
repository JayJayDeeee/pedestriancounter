
#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/flann/flann.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <stack>

typedef pthread_mutex_t MUTEX;
typedef pthread_t THREAD;

using namespace cv;
using namespace std;

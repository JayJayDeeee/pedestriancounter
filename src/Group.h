#pragma once

#include "Common.h"
#include "MyPoint.h"

class Group
{
private:
    vector<MyPoint> mList;
public:
    Group () {  }
    void add ( MyPoint p ) { mList.push_back ( p ); }
    MyPoint get ( int position ) { return mList[position]; }
    Rect getRect ( void );
    Rect getRect2 ( void );
    void clusterize ( vector<Group> &groupList , double threshold );
};

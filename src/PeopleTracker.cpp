#include "PeopleTracker.h"
#include "EException.h"
#include "HogVerifier.h"
#include <limits>

PeopleTracker::PeopleTracker ( Rect roiCandidate , Rect roi , Size frameSize , HogVerifier *pVerifier )
{
	m_candidateRoi = roiCandidate;
	m_trackingRoi = roi;
	m_frameMaxLength =
			sqrt(frameSize.width*frameSize.width +frameSize.height*frameSize.height);
	m_tagIndex = 0;
	m_pVerifier = pVerifier;
}

int PeopleTracker::countAndremoveTags ( void )
{
	int i; int count = 0;
	vector< vector<PeopleTag>::iterator > delList;
	vector<PeopleTag>::iterator itr = m_peoples.begin ();

	// PRIMARY TAG가 Candidate ROI에 있으면 카운트 증가
	for ( i = 0 ; i < (int)m_peoples.size() ; i++ )
	{
		if ( m_peoples[i].getTagType() == PeopleTag::TAG_PRIMARY &&
				isRectContains ( m_candidateRoi , m_peoples[i].getLocation() ) == true &&
				isRectContains ( m_trackingRoi , m_peoples[i].getLocation() ) == false )
		{
			m_peoples[i].increasePostPassCount();
		}
	}

	for ( i = 0 ; i < (int)m_peoples.size() ; i++ )
	{
		// CASE1 : SECONDARY TAG가 cadidate ROI 밖으로 나가면 삭제
		if ( isRectContains ( m_candidateRoi , m_peoples[i].getLocation() ) == false )
		{ delList.push_back ( itr ); itr++; continue; }

		/*
		// CASE2 : PRIMARY TAG에 한해 PrevCentroid가 trakingROI 안에 있으며
		//			CurCentroid가 candidateROI 안에 존재할 경우
		if ( isRectContains ( m_trackingRoi , m_peoples[i].getPrevLocation() ) == true &&
				isRectContains ( m_candidateRoi , m_peoples[i].getLocation() ) == true &&
				isRectContains ( m_trackingRoi , m_peoples[i].getLocation() ) == false &&
				m_peoples[i].getTagType() == PeopleTag::TAG_PRIMARY )
		{
			if ( getDistance(m_peoples[i].getPrevLocation(),
								m_peoples[i].getLocation()) >= 20 )
			{ itr++; continue; }

			//delList.push_back ( itr );
			//count++;
			itr++;
			continue;
		}

		// CASE3 : CASE2에 걸리지 않은 경우
		else if ( isRectContains ( m_candidateRoi , m_peoples[i].getLocation() ) == true &&
					isRectContains ( m_trackingRoi , m_peoples[i].getLocation() ) == false &&
					m_peoples[i].getTagType() == PeopleTag::TAG_PRIMARY )
		{
			if ( getDistance(m_peoples[i].getPrevLocation(),
									m_peoples[i].getLocation()) >= 20 )
			{ itr++; continue; }

			//delList.push_back ( itr );
			//count++;

			itr++;
			continue;
		}
		*/

		// CASE4 : SECONDARY TAG이면서 PREDICT COUNT가 10이 넘어갈 경우 삭제한다.
		else if (
					m_peoples[i].getPredictCount() > PREDICT_COUNT_THRESHOLD )
		{
			delList.push_back ( itr );
			itr++;
			continue;
		}

		// CASE5 : HOGTTL : Hog-TTL이 0 인 경우 삭제한다.
		else if ( m_peoples[i].getHogTTL() <= 0 )
		{
			delList.push_back ( itr );
			itr++;
			continue;
		}

		else if ( m_peoples[i].getTagType () == PeopleTag::TAG_PRIMARY &&
					m_peoples[i].getPostPassCount() >= POSTPASS_THRESHOLD )
		{
			delList.push_back ( itr );
			count++;

			itr++;
			continue;
		}

		else if ( m_peoples[i].getTagType() == PeopleTag::TAG_SECONDARY &&
					isRectContains ( m_trackingRoi , m_peoples[i].getLocation() ) == true )
		{
			delList.push_back ( itr );

			itr++;
			continue;
		}

		itr++;
	}

	for ( i = 0 ; i < (int)delList.size() ; i++ ) { m_peoples.erase ( delList[i] ); }
	//cout << "TAG COUNT : " << m_peoples.size() << endl;

	return count;
}

void PeopleTracker::getResults ( vector<SIMPLETAG> &results )
{
	results.clear ();
	int i;
	for ( i = 0 ; i < (int)m_peoples.size() ; i++ )
	{
		SIMPLETAG tag;
		tag.m_location = m_peoples[i].getLocation();
		tag.m_tag = m_peoples[i].getTag();
		tag.m_tagType = m_peoples[i].getTagType();
		tag.m_meanWndSize = m_peoples[i].getMeanWindowSize();
		tag.m_trackState = m_peoples[i].getTrackState();

		results.push_back ( tag );
	}
}

void PeopleTracker::update ( Mat frame , vector<Rect> &hogArea , vector<Rect> &denseMovingBlob )
{
	int i; int j;
	vector<Rect> copiedHogArea;
	Mat hogHuman;

	// 호그영역에 대한 복사본 생성
	copiedHogArea.resize ( hogArea.size() );
	copiedHogArea.assign ( hogArea.begin() , hogArea.end() );

	// 현재 있는 사람영역 하나에 대하여 새로온 호그영역을 찾고 매칭되면 현재 있는 사람영역에 대해 update하고
	// 새로 온 호그영역의 피매칭된 새끼를 지운다.
	int denseMovingBlobSize = (int)denseMovingBlob.size();
	Rect predictedPeopleRect;
	Point predictedPeopleCentroid;
	Size predictedPeopleSize;

	for ( i = 0 ; i < (int)m_peoples.size() ; i++ )
	{
		Point tagCentroid = m_peoples[i].getLocation();
		double maxScore = 0;
		vector<Rect>::iterator itr = copiedHogArea.begin ();
		vector<Rect>::iterator endItr = copiedHogArea.end ();
		vector<Rect>::iterator delItr = copiedHogArea.end (); //삭제대상 이터레이터

		Point hogCentroid;
		double *distanceArray = new double[copiedHogArea.size()];
		double minDistance = numeric_limits<double>::max();
		for ( j = 0 ; j < (int)copiedHogArea.size() ; j++ ) {
			Point hogCentroid = Point ( copiedHogArea[j].x + copiedHogArea[j].width/2 ,
											copiedHogArea[j].y + copiedHogArea[j].height/2 );
			distanceArray[j] = getDistance ( tagCentroid , hogCentroid );
			if(distanceArray[j] < minDistance) { minDistance = distanceArray[j]; }
		}

		for ( j = 0 ; j < (int)copiedHogArea.size() ; j++ )
		{
			if ( distanceArray[j] > DISTANCE_THRESHOLD ) { itr++; continue; }

			Mat tagMat; m_peoples[i].copyTo(tagMat);

			// ( PeopleTag *pTag , Mat frame , Rect rect , double alpha );

			double hogAlpha = SIMILARITY_ALPHA;
			if(m_peoples[i].isCollided() == true) { hogAlpha = SIMILARITY_ALPHA_OCCLUSION; }
			double score = getSimilarity ( &(m_peoples[i]) , frame , copiedHogArea[j] , minDistance , hogAlpha );

			if ( maxScore <= score ) { maxScore = score; delItr = itr; }
			itr++;
		}
		delete[] distanceArray;

		if ( maxScore <= SIMILARITY_THRESHOLD || delItr == endItr )
		{
			m_peoples[i].updatePrediction();
			Point predictPt = m_peoples[i].getLocation();
			Size predictWndSize = m_peoples[i].getMeanWindowSize();

			Rect predictRect = Rect ( predictPt.x - predictWndSize.width/2 ,
										predictPt.y - predictWndSize.height/2 ,
										predictWndSize.width , predictWndSize.height );

			try { predictRect = normalizeRect (Size(frame.cols,frame.rows),predictRect); }
			catch ( EException &ex ) { cout << ex.getMessage() << endl; continue; }

			Mat predictMat; Mat(frame,predictRect).copyTo(predictMat);
			Mat tagMat; m_peoples[i].copyTo(tagMat);

			double predictAlpha = SIMILARITY_ALPHA;
			if(m_peoples[i].isCollided() == true) { predictAlpha = SIMILARITY_ALPHA_OCCLUSION; }
			double score = getCorrelation(tagMat , predictMat);
					//Similarity ( &(m_peoples[i]) , frame , predictRect , minDistance , predictAlpha );

			// 2ND : PREDICTION POINT의 이미지 유사도 비교!!
			if ( score >= SIMLARITY_PREDICT_THRESHOLD )
			{
				//m_peoples[i].updateMeasurement(predictPt);
				m_peoples[i].updateFinal();
				m_peoples[i].decreaseHogTTL(); /// HOGTTL:HoG로 measurement 한것이 아니므로 TTL을 하나 감소시킨다!!
				m_peoples[i].setTrackState(PeopleTag::STATE_MEASUREMENT_KALMAN);

				// Candidate ROI -> Tracking ROI 로 이행할 경우 태그를 PRIMARY TAG로 승급한다.
				if ( isRectContains(m_candidateRoi,m_peoples[i].getPrevLocation()) == true &&
						isRectContains(m_trackingRoi,m_peoples[i].getPrevLocation()) == false &&
						isRectContains(m_trackingRoi,m_peoples[i].getLocation()) == true )
				{
					m_peoples[i].setPostPassCount(0);
					m_peoples[i].setTagType(PeopleTag::TAG_PRIMARY);
				}
				continue;

			} else {
				m_peoples[i].updateFinal();
				m_peoples[i].setTrackState(PeopleTag::STATE_PREDICTION);
				if ( isRectContains(m_candidateRoi,m_peoples[i].getPrevLocation()) == true &&
						isRectContains(m_trackingRoi,m_peoples[i].getPrevLocation()) == false &&
						isRectContains(m_trackingRoi,m_peoples[i].getLocation()) == true )
				{
					m_peoples[i].setPostPassCount ( 0 );
					m_peoples[i].setTagType(PeopleTag::TAG_PRIMARY);
				}
				continue;
			}

			// Candidate ROI -> Tracking ROI 로 이행할 경우 태그를 PRIMARY TAG로 승급한다.
			if ( isRectContains(m_candidateRoi,m_peoples[i].getPrevLocation()) == true &&
					isRectContains(m_trackingRoi,m_peoples[i].getPrevLocation()) == false &&
					isRectContains(m_trackingRoi,m_peoples[i].getLocation()) == true )
			{
				m_peoples[i].setPostPassCount(0);
				m_peoples[i].setTagType(PeopleTag::TAG_PRIMARY);
			}
			continue;
		}

		Rect r = *(delItr);

		Mat(frame , r).copyTo(hogHuman);
		Point newHogCentroid ( delItr->x + delItr->width/2 ,
											delItr->y + delItr->height/2 );

		// 1ST : HoG 중심점 사용
		m_peoples[i].updatePrediction();
		m_peoples[i].updateMeasurement ( newHogCentroid );
		m_peoples[i].updateFinal();
		m_peoples[i].setHogTTL ( HOG_TTL ); // HOGTTL : HoG가 일어났으므로 TTL을 원복한다.
		m_peoples[i].setTrackState(PeopleTag::STATE_MEASUREMENT_HOG);

		// 해당 PeopleTag의 Window가 Occlusion이 일어나지 않은 경우에 한해서만 Image Update 수행
		if ( m_peoples[i].isCollided() == false )
		{
			m_peoples[i].copyFrom ( hogHuman );
		}
		m_peoples[i].setMeanWindowSize ( Size(r.width,r.height) );

		// Candidate ROI -> Tracking ROI 로 이행할 경우 태그를 PRIMARY TAG로 승급한다.
		if ( isRectContains(m_candidateRoi,m_peoples[i].getPrevLocation()) == true &&
				isRectContains(m_trackingRoi,m_peoples[i].getPrevLocation()) == false &&
				isRectContains(m_trackingRoi,m_peoples[i].getLocation()) == true )
		{
			m_peoples[i].setPostPassCount(0);
			m_peoples[i].setTagType(PeopleTag::TAG_PRIMARY);
		}

		copiedHogArea.erase ( delItr );
	}

	// 여기까지 왔다는 건 새로들어온 호그영역이 매칭되지 않았다는 것이므로
	// vector copiedHogArea 안에 있는놈들을 모두 새로 생성하면 된다.
	for ( i = 0 ; i < (int)copiedHogArea.size () ; i++ )
	{
		Rect peopleRect = copiedHogArea[i];
		Point peopleCentroid ( peopleRect.x + peopleRect.width/2 ,
									peopleRect.y + peopleRect.height/2 );

		/// CandidateArea 안에 속할경우만 생성, 그리고 TrackingArea안일경우 생성하지 않는다.
		if ( isRectContains ( m_candidateRoi , peopleCentroid ) == false )	{ continue; }
		//else if ( isRectContains ( m_trackingRoi , peopleCentroid ) == true ) { continue; }

		Size peopleSize ( peopleRect.width , peopleRect.height );
		Mat peopleMat; Mat(frame,peopleRect).copyTo(peopleMat);

		//if ( m_pVerifier->isCompleted() == false ) { m_pVerifier->addImage( peopleMat ); continue; }
		//else if ( m_pVerifier->isValidHog(peopleMat) == false ) { continue; }


		int tagType = PeopleTag::TAG_SECONDARY;
		//if( isRectContains ( m_trackingRoi , peopleCentroid ) == true ) { tagType = PeopleTag::TAG_PRIMARY; }
		PeopleTag newtag ( peopleSize , peopleCentroid , peopleMat );
		newtag.setTag(generateId());
		newtag.setTagType(tagType);
		newtag.updateMeasurement ( peopleCentroid );
		newtag.updateFinal();
		newtag.setMeanWindowSize(peopleSize);

		m_peoples.push_back(newtag);
	}

	updateByNoTagHog( frame, hogArea );
	updateOcclusionStatus ();
}

bool PeopleTracker::isRectContains ( Rect area , Point pt )
{
	if ( pt.x > area.x &&
			pt.x < area.x + area.width &&
			pt.y > area.y &&
			pt.y < area.y + area.height )
	{
		return true;
	}
	return false;
}

string PeopleTracker::generateId ( void )
{
	char ch = 'A' + (char)m_tagIndex;
	string levelExpr = "";

	m_tagIndex++;
	if ( m_tagIndex == 52 ) { m_tagIndex = 0; }

	string ret = ""; ret += ch;
	return ret;
}

double PeopleTracker::getSimilarity ( PeopleTag *pPrevTag , Mat frame , Rect rect , double minDistance, double alpha )
{
	Mat prevHuman; pPrevTag->copyTo(prevHuman);
	Mat curHuman; Mat(frame,rect).copyTo(curHuman);
	double corr = getCorrelation ( prevHuman , curHuman );

	Point prevCentroid = pPrevTag->getLocation();
	Point curCentroid = Point ( rect.x + rect.width/2 , rect.y + rect.height/2 );

	double distRate = getDistanceRate ( prevCentroid , curCentroid , minDistance);
	return ((double)1-alpha)*corr + alpha*distRate;
}

double PeopleTracker::getCorrelation ( Mat mat1 , Mat mat2 )
{
	double hist = 0.0f;
	Mat mat1hist;
	Mat mat2hist;

	int channels[3] = { 0, 1, 2 }; // 0th & 1st channel
	int histSize[3] = { 60, 60, 60 };
	float rRanges[] = { 0, 256 };
	float gRanges[] = { 0, 256 };
	float bRanges[] = { 0, 256 };
	const float* ranges[] = { rRanges, gRanges, bRanges };

	calcHist ( &mat1 , 1 , channels , Mat() , mat1hist , 2 , histSize , ranges , true , false );
	calcHist ( &mat2 , 1 , channels , Mat() , mat2hist , 2 , histSize , ranges , true , false );
	normalize ( mat1hist , mat1hist , 0 , 1 , NORM_MINMAX , -1 , Mat() );
	normalize ( mat2hist , mat2hist , 0 , 1 , NORM_MINMAX , -1 , Mat() );

	hist = compareHist ( mat1hist , mat2hist , CV_COMP_CORREL );
	return hist;
}

double PeopleTracker::getDistanceRate ( Point pt1 , Point pt2 , double minDistance )
{
	double ret = 0;
	double ptLength =
			sqrt ( (pt2.x-pt1.x)*(pt2.x-pt1.x) + (pt2.y-pt1.y)*(pt2.y-pt1.y) );
	//ret = ((double)1 - ptLength / maxDistance );
	ret = minDistance/ptLength;



	cout << ret << endl;
	return ret;
}

double PeopleTracker::getDistance ( Point pt1 , Point pt2 )
{
	double dist = (double)sqrt ( ((double)pt2.x-(double)pt1.x)*((double)pt2.x-(double)pt1.x) +
			((double)pt2.y-(double)pt1.y)*((double)pt2.y-(double)pt1.y) );
	return dist;
}

void PeopleTracker::truncateCentroid ( const vector<Rect> hogAreas ,  vector<Rect> &results ,
											vector<FINDSTATE> &flags , Point pt , double threshold )
{
	flags.clear ();
	int i;

	for ( i = 0 ; i < (int)hogAreas.size() ; i++ )
	{
		Rect hogArea = hogAreas[i];
		Point hogCentroid = Point ( hogArea.x+hogArea.width/2 ,
										hogArea.y+hogArea.height/2 );
		double distance =
				sqrt ( (hogCentroid.x-pt.x)*(hogCentroid.x-pt.x) +
							(hogCentroid.y-pt.y)*(hogCentroid.y-pt.y) );
		if ( distance <= threshold ) { results.push_back ( hogArea ); flags[i]=FINDSTATE_TRUNCATED; }
	}
}

Rect PeopleTracker::normalizeRect ( Size frameSize , Rect srcRect )
{
	if ( srcRect.width > frameSize.width ) { throw EException("WIDTH OVER"); }
	if ( srcRect.height > frameSize.height ) { throw EException("HEIGHT OVER"); }

	int x = srcRect.x; if ( srcRect.x < 0 ) { x = 0; } if ( srcRect.x > frameSize.width ) { throw EException("ERR"); }
	int y = srcRect.y; if ( srcRect.y < 0 ) { y = 0; } if ( srcRect.y > frameSize.height ) { throw EException("ERR"); }
	int w = srcRect.width; if ( x + srcRect.width > frameSize.width ) { w=frameSize.width-srcRect.x; }
	int h = srcRect.height; if ( y + srcRect.height > frameSize.height ) { h=frameSize.height-srcRect.y; }
	Rect ret = Rect ( x , y , w , h );

	if ( ret.width <= 0 ) { throw EException("WIDTH0"); }
	if ( ret.height <= 0 ) { throw EException("HEIGHT0"); }

	return ret;
}

void PeopleTracker::updateOcclusionStatus ( void )
{
	int i; int j; string expr = "";
	for ( i = 0 ; i < (int)m_peoples.size() ; i++ )
	{
		if ( m_peoples[i].getTagType() != PeopleTag::TAG_PRIMARY ) { continue; }

		Rect srcRect = Rect ( m_peoples[i].getLocation().x - m_peoples[i].getMeanWindowSize().width/2 ,
								m_peoples[i].getLocation().y - m_peoples[i].getMeanWindowSize().height/2 ,
								m_peoples[i].getMeanWindowSize().width ,
								m_peoples[i].getMeanWindowSize().height );
		double coeff = 0.0;

		for ( j = 0 ; j < (int)m_peoples.size() ; j++ )
		{
			if ( i == j ) { continue; }

			if ( m_peoples[j].getTagType() != PeopleTag::TAG_PRIMARY ) { continue; }

			Rect dstRect = Rect ( m_peoples[j].getLocation().x - m_peoples[j].getMeanWindowSize().width/2 ,
											m_peoples[j].getLocation().y - m_peoples[j].getMeanWindowSize().height/2 ,
											m_peoples[j].getMeanWindowSize().width ,
											m_peoples[j].getMeanWindowSize().height );

			coeff = getOcclusionCoefficient ( srcRect , dstRect );
			if ( coeff > OCCLUSION_THRESHOLD )
			{
				m_peoples[i].setIsCollided(true);
				m_peoples[j].setIsCollided(true);
			}
		}

		string tfExpr = "t";
		if ( m_peoples[i].isCollided() == true ) { tfExpr="T"; } else { tfExpr="F"; }

		expr += m_peoples[i].getTag() + ":[" + tfExpr + "] ";
	}
	cout << expr << endl;
}

double PeopleTracker::getOcclusionCoefficient2 ( Rect srcRect , Rect dstRect )
{
	double ret = 0.0;

	return ret;
}

double PeopleTracker::getOcclusionCoefficient ( Rect srcRect , Rect dstRect )
{
	double ret = 0.0; double totalArea = 0.0;

	totalArea = (double)srcRect.width * (double)srcRect.height +
					(double)dstRect.width * (double)dstRect.height;
	double commonArea = 0.0;
	int commonWidth = 0; int commonHeight = 0;

	if ( dstRect.x > srcRect.x &&
			dstRect.x < srcRect.x + srcRect.width &&
			dstRect.y > srcRect.y &&
			dstRect.y < srcRect.y + srcRect.height )
	{
		commonWidth = srcRect.x + srcRect.width - dstRect.x;
		commonHeight = srcRect.y + srcRect.height - dstRect.y;
	}
	else if ( dstRect.x+dstRect.width > srcRect.x &&
			dstRect.x+dstRect.width < srcRect.x+srcRect.width &&
			dstRect.y > srcRect.y &&
			dstRect.y < srcRect.y+srcRect.height )
	{
		commonWidth = dstRect.x + dstRect.width - srcRect.x;
		commonHeight = srcRect.y + srcRect.height - dstRect.y;
	}
	else if ( dstRect.x+dstRect.width > srcRect.x &&
			dstRect.x+dstRect.width < srcRect.x+srcRect.width &&
			dstRect.y+dstRect.height > srcRect.y &&
			dstRect.y+dstRect.height < srcRect.y+srcRect.height )
	{
		commonWidth = dstRect.x + dstRect.width - srcRect.x;
		commonHeight = dstRect.y + dstRect.height - srcRect.y;
	}
	else if ( dstRect.x > srcRect.x &&
			dstRect.x < srcRect.x+srcRect.width &&
			dstRect.y+dstRect.height > srcRect.y &&
			dstRect.y+dstRect.height > srcRect.y+srcRect.height )
	{
		commonWidth = srcRect.x + srcRect.width - dstRect.x;
		commonHeight = dstRect.y + dstRect.height - srcRect.y;
	}
	else { commonWidth=0; commonHeight=0; }

	if ( dstRect.x == srcRect.x && dstRect.y == srcRect.y &&
			dstRect.width == srcRect.width && dstRect.height == srcRect.height )
	{ return 1.0; }

	commonArea = (double)commonWidth * (double)commonHeight;

	ret = (double)commonArea / ((double)totalArea/2);
	return ret;
}

bool PeopleTracker::isRectCollided ( Rect srcRect , Rect dstRect )
{
	if ( dstRect.x > srcRect.x &&
			dstRect.x < srcRect.x + srcRect.width &&
			dstRect.y > srcRect.y &&
			dstRect.y < srcRect.y + srcRect.height )
	{ return true; }

	else if ( dstRect.x+dstRect.width > srcRect.x &&
				dstRect.x+dstRect.width < srcRect.x+srcRect.width &&
				dstRect.y > srcRect.y &&
				dstRect.y < srcRect.y+srcRect.height )
	{ return true; }

	else if ( dstRect.x+dstRect.width > srcRect.x &&
				dstRect.x+dstRect.width < srcRect.x+srcRect.width &&
				dstRect.y+dstRect.height > srcRect.y &&
				dstRect.y+dstRect.height < srcRect.y+srcRect.height )
	{ return true; }

	else if ( dstRect.x > srcRect.x &&
				dstRect.x < srcRect.x+srcRect.width &&
				dstRect.y+dstRect.height > srcRect.y &&
				dstRect.y+dstRect.height > srcRect.y+srcRect.height )
	{ return true; }

	return false;
}

void PeopleTracker::updateByNoTagHog ( Mat frame , const vector<Rect> &hogAreas ) {
	Rect checkArea;
	bool isCheckRectContain = false;
	bool isTrackRoiContain = false;
	int tagType = 0;
	Size frameSize = Size(frame.cols, frame.rows);
	vector<Rect>::size_type hogAreasSize = hogAreas.size();
	vector<Rect>::size_type m_peoplesSize = m_peoples.size();

	int *hogCheckArray = new int[hogAreasSize];
	for(vector<Rect>::size_type i = 0; i < hogAreasSize; i++) {
		hogCheckArray[i] = 0;
	}

	Point *hogCenterPointArray = new Point[hogAreasSize];
	for(vector<Rect>::size_type i = 0; i < hogAreasSize; i++) {
		hogCenterPointArray[i] = Point( (hogAreas[i].x + hogAreas[i].width/2),
								   (hogAreas[i].y + hogAreas[i].height/2) );
		isTrackRoiContain = isRectContains(m_trackingRoi, hogCenterPointArray[i]);
		if(isTrackRoiContain == false) { continue; }

		checkArea = Rect( (hogCenterPointArray[i].x - CHECK_AREA_WIDTH/2),
						    (hogCenterPointArray[i].y - CHECK_AREA_HEIGHT/2),
							 CHECK_AREA_WIDTH, CHECK_AREA_HEIGHT);
		try { checkArea = normalizeRect( frameSize, checkArea ); }
		catch ( EException &ex ) { cout << ex.getMessage() << endl; continue; }

		for(vector<Rect>::size_type j = 0; j < m_peoplesSize; j++) {
			tagType = m_peoples[j].getTagType();
			if(tagType != PeopleTag::TAG_PRIMARY) { continue; }

			isCheckRectContain = isRectContains ( checkArea, m_peoples[j].getLocation() );
			if(isCheckRectContain == true) { hogCheckArray[i]++; }
		}
	}

	Size peopleSize;
	Mat peopleMat;
	for(vector<Rect>::size_type i = 0; i < hogAreasSize; i++) {
		if(hogCheckArray[i] != 0) { continue; }
		isTrackRoiContain = isRectContains(m_trackingRoi, hogCenterPointArray[i]);
		if(isTrackRoiContain == false) { continue; }

		Mat(frame, hogAreas[i]).copyTo(peopleMat);
		peopleSize = Size(hogAreas[i].width, hogAreas[i].height);
		PeopleTag newtag ( peopleSize , hogCenterPointArray[i] , peopleMat );
		newtag.setTag(generateId());
		newtag.setTagType(PeopleTag::TAG_PRIMARY);
		newtag.updateMeasurement ( hogCenterPointArray[i] );
		newtag.updateFinal();
		newtag.setMeanWindowSize(peopleSize);

		m_peoples.push_back(newtag);
	}


	delete[] hogCheckArray;
	delete[] hogCenterPointArray;
}

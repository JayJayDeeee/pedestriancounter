#pragma once

#include "Common.h"
#include <pthread.h>

class HumanFastFinder
{
	public:
		void findHumans ( Mat mat , vector<Rect> &humanAreas , CascadeClassifier *pClassifier );
		void findHumansSingle ( Mat frame , vector<Rect> pieces , vector<Rect> &humanAreas , CascadeClassifier *pClassifier );
		void findHumansMultiple ( Mat frame , vector<Rect> pieces , vector<Rect> &humanAreas , FileStorage *pFs );
};

typedef struct _HOGINFO
{
	Mat m_frame;
	Rect m_rect;
	FileStorage *m_pFs;
}HOGINFO;

typedef struct _HOGRESULT
{
	vector<Rect> m_result;
}HOGRESULT;

void *hogFindProc ( void *param );

#include "PeopleTag.h"

PeopleTag::PeopleTag ()
{
	m_countSum = 0;
	m_tagType = TAG_SECONDARY;
	m_isFirst = true;
	m_isCollided = false;
	m_trackState = STATE_NONE;

	m_hogTTL = HOG_TTL;
	m_predictCount = 0;

	m_postPassCount = 0;
}

void PeopleTag::setInitValue ( Size windowSize , Point location , Mat image )
{
	m_countSum = 0;
	m_initKalmanCount = 0;
	m_tagType = TAG_SECONDARY;
	m_isFirst = true;
	m_isCollided = false;
	m_trackState = STATE_NONE;

	m_predictCount = 0;
	m_hogTTL = HOG_TTL;

	m_postPassCount = 0;

	m_windowSize = windowSize;
	m_location = location;
	image.copyTo ( m_image );

	m_kf = KalmanFilter ( 4 , 2 , 0 );

	m_kf.statePre.at<float>(0) = location.x;
	m_kf.statePre.at<float>(1) = location.y;
	m_kf.statePre.at<float>(2) = 0;
	m_kf.statePre.at<float>(3) = 0;

	m_kf.transitionMatrix =
			*(Mat_<float>(4,4) <<
					1,0,DT,0,
					0,1,0,DT,
					0,0,1,0,
					0,0,0,1 );

	setIdentity ( m_kf.measurementMatrix );
	setIdentity ( m_kf.processNoiseCov , Scalar::all(1e-4) );
	setIdentity ( m_kf.measurementNoiseCov , Scalar::all(1e-4) );
	setIdentity ( m_kf.errorCovPost , Scalar::all(1e-4) );
	m_countSum = 0;
}

void PeopleTag::setMeanWindowSize ( Size size )
{
	m_windowSizeSum.width += size.width;
	m_windowSizeSum.height += size.height;
	m_countSum++;
}

Size PeopleTag::getMeanWindowSize ( void )
{
	Size ret = Size ( m_windowSizeSum.width / m_countSum ,
						m_windowSizeSum.height / m_countSum );
	return ret;
}

void PeopleTag::updatePrediction ( void )
{
	Mat prediction = m_kf.predict();

	Point ret = Point ( prediction.at<float>(0) , prediction.at<float>(1) );
	m_candidateLocation = ret;

	//if ( m_isFirst == true ) { m_prevLocation = m_location; m_isFirst = false; }

	m_predictCount++;
}

void PeopleTag::updateMeasurement ( Point pt )
{
	Mat_<float> measurement(2,1);
	measurement.setTo(Scalar(0));

	measurement(0) = pt.x;
	measurement(1) = pt.y;

	Mat estimated = m_kf.correct ( measurement );

	Point ret = Point ( estimated.at<float>(0) , estimated.at<float>(1) );
	m_candidateLocation = ret;
	m_predictCount = 0;
}

void PeopleTag::updateFinal()
{
	if ( m_isFirst == true ) {
		m_prevLocation = m_location;
		m_isFirst = false;
		return;
	}

	m_prevLocation = m_location;
	m_location = m_candidateLocation;
}

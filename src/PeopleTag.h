#pragma once

#include "Common.h"

#define HOG_TTL 13
#define DT	1

class PeopleTag
{
	public:
		const static int TAG_SECONDARY = 0;
		const static int TAG_PRIMARY = 1;

		const static int STATE_MEASUREMENT_HOG = 0;
		const static int STATE_MEASUREMENT_KALMAN = 1;
		const static int STATE_MEASUREMENT_MOVINGBLOB = 2;
		const static int STATE_PREDICTION = 3;
		const static int STATE_NONE = -1;

	private:
		int				m_trackState;
		string			m_tag;
		Mat				m_image;

		Point			m_prevLocation;
		Point			m_location;
		Point			m_candidateLocation;

		Size			m_windowSize;
		KalmanFilter	m_kf;
		int				m_tagType;

		Size			m_windowSizeSum;
		int				m_countSum;
		bool			m_isFirst;
		int				m_initKalmanCount;

		bool			m_isCollided;		// 현재 Occlusion이 발생한 태그인지의 여부
		int				m_predictCount;		// 연속 Prediction count 횟수 => Measurement시 0으로 만든다.
		int				m_hogTTL;			// HoG영역이 잡히지 않을때 마다 TTL감소 => Hog False Positive 제거용

		int				m_postPassCount;

		int				m_lastFrame;

	public:
		PeopleTag ();
		PeopleTag ( Size windowSize , Point location , Mat image )
					{ setInitValue ( windowSize , location , image ); }

	public:
		int	getPostPassCount ( void ) { return m_postPassCount; }
		void setPostPassCount ( int postPassCount ) { m_postPassCount = postPassCount; }
		void increasePostPassCount ( void ) { m_postPassCount++; }

		void setIsCollided ( bool isCollide ) { m_isCollided = isCollide; }
		bool isCollided ( void ) { return m_isCollided; }

		void copyFrom ( Mat mat ) { mat.copyTo ( m_image ); }
		void copyTo ( Mat &mat ) { m_image.copyTo ( mat ); }
		string getTag ( void ) { return m_tag; }
		void setTag ( string tag ) { m_tag = tag; }
		int getTagType ( void ) { return m_tagType; }
		void setTagType ( int tagType ) { m_tagType = tagType; }

		int getTrackState ( void ) { return m_trackState; }
		void setTrackState ( int trackState ) { m_trackState = trackState; }

		void setWindowSize ( Size size ) { m_windowSize = size; }
		Size getWindowSize ( void ) { return m_windowSize; }

		void setMeanWindowSize ( Size size );
		Size getMeanWindowSize ( void );

		int getCount() { return m_countSum; }
		int getPredictCount () { return m_predictCount; }

		Point getLocation ( void ) { return m_location; }
		Point getPrevLocation ( void ) { return m_prevLocation; }

		void updatePrediction ( void );
		void updateMeasurement ( Point pt );

		void decreaseHogTTL ( void ) { if ( m_hogTTL > 0 ) { m_hogTTL--; } }
		void setHogTTL ( int hogTTL ) { m_hogTTL = hogTTL; }
		int getHogTTL ( void ) { return m_hogTTL; }

		void updateFinal();

		void setInitValue ( Size windowSize , Point location , Mat image );
};

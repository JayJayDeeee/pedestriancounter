#pragma once

#include "Common.h"
#include "PeopleTag.h"
#include "HogVerifier.h"

typedef struct _SIMPLETAG
{
	Point m_location;
	string m_tag;
	int m_tagType;
	Size m_meanWndSize;
	int m_trackState;
}SIMPLETAG;


//이미지가 겹치는지 확인할 떄 사용
#define OCCLUSION_THRESHOLD 0.1

//너무 멀리 있는 이미지는 태그 추적시 빠지도록
#define DISTANCE_THRESHOLD 60.0

//이 이미지와 현재 받은 HOG값이 얼마만큼 닮았는가 확인
#define SIMILARITY_THRESHOLD 0.75

//이 태그와 예상하는 좌표값을 기준으로 만들어진 RECT속의 이미지가 얼마만큼 닮았는가 확인
#define SIMLARITY_PREDICT_THRESHOLD 0.95

//서로 비교할 경우에 사용하는 알파값
#define SIMILARITY_ALPHA 0.4
#define SIMILARITY_ALPHA_OCCLUSION 0.2

//지울 경우에 사용
#define POSTPASS_THRESHOLD 5
#define PREDICT_COUNT_THRESHOLD 8

#define CHECK_AREA_WIDTH 130
#define CHECK_AREA_HEIGHT 130
/*
//이미지가 겹치는지 확인할 떄 사용
#define OCCLUSION_THRESHOLD 0.1

//너무 멀리 있는 이미지는 태그 추적시 빠지도록
#define DISTANCE_THRESHOLD 80.0


//백업
//이 이미지와 현재 받은 HOG값이 얼마만큼 닮았는가 확인
#define SIMILARITY_THRESHOLD 0.8

//이 태그와 예상하는 좌표값을 기준으로 만들어진 RECT속의 이미지가 얼마만큼 닮았는가 확인
#define SIMLARITY_PREDICT_THRESHOLD 0.8

//서로 비교할 경우에 사용하는 알파값
#define SIMILARITY_ALPHA 0.4
#define SIMILARITY_ALPHA_OCCLUSION 0.1

//지울 경우에 사용
#define POSTPASS_THRESHOLD 5
#define PREDICT_COUNT_THRESHOLD 8

#define CHECK_AREA_WIDTH 130
#define CHECK_AREA_HEIGHT 130
*/

enum FINDSTATE
{
	FINDSTATE_NONE = -1,
	FINDSTATE_MOVED = 0,
	FINDSTATE_TRUNCATED = 1,
	FINDSTATE_MISSED = 2
};

class PeopleTracker
{
	private:
		Rect m_candidateRoi;
		Rect m_trackingRoi;

		vector<PeopleTag> m_peoples;
		vector<SIMPLETAG> m_results;

		vector<Mat> m_trains;

		double m_frameMaxLength;
		int m_tagIndex;

		HogVerifier *m_pVerifier;

	public:
		PeopleTracker ( Rect roiCandidate , Rect roi , Size frameSize , HogVerifier *pVerifier );

	public:
		void update ( Mat frame , vector<Rect> &hogArea , vector<Rect> &movingBlob );
		void getResults ( vector<SIMPLETAG> &results );
		int countAndremoveTags ( void );

	private:
		double	getSimilarity ( PeopleTag *pTag , Mat frame , Rect rect , double minDistance, double alpha );
		double	getCorrelation ( Mat mat1 , Mat mat2 );
		double	getDistanceRate ( Point pt1 , Point pt2, double minDistance );
		double	getDistance ( Point pt1 , Point pt2 );
		void	truncateCentroid ( const vector<Rect> hogAreas ,  vector<Rect> &tags ,
										vector<FINDSTATE> &flags , Point pt , double threshold );
		Rect 	normalizeRect ( Size frameSize , Rect srcRect );
		string 	generateId ( void );

		void	generatePeopleTag ( Mat frame , vector<Rect> &copiedPeopleTag );
		bool	isRectContains ( Rect area , Point pt );
		void	updateOcclusionStatus ( void );
		void	updateByNoTagHog ( Mat frame , const vector<Rect> &peopleTags );

		bool	isRectCollided ( Rect srcRect , Rect dstRect );

		double	getOcclusionCoefficient ( Rect rectA , Rect rectB );
		double	getOcclusionCoefficient2 ( Rect srcRect , Rect dstRect );
};

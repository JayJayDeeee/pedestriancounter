#pragma once

#include "Common.h"

class HogVerifier
{
	private:
		int m_maxCount;
		vector<Mat> m_targetMats;

	public:
		HogVerifier ( int targetCount );

	public:
		int		getImageCount ( void ) { return m_targetMats.size(); }
		void	addImage ( Mat &mat );

		bool	isCompleted ( void ) { if((int)m_targetMats.size()>=m_maxCount){return true;} return false; }
		bool	isValidHog ( Mat &mat );

	private:
		double	getCorrelation ( Mat mat1 , Mat mat2 );
};

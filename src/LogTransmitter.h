#pragma once

#include "Common.h"

#include <sys/socket.h>
#include <curl/curl.h>
#include <curl/easy.h>
#include <curl/curlrules.h>

#define SERVER_ID "00001"
#define SERVER_ADDR "http://gomgogi.com:22223"
#define SEND_INTERVAL 5

typedef struct _LOG
{
	int m_peopleCount;
	Mat m_mat;
}LOG;

static const string g_base64_chars =
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz"
			"0123456789+/";

class LogTransmitter
{
	private:
		int m_prevSecCount;
		int m_prevFrameIdx;

	public:
		LogTransmitter();
		virtual ~LogTransmitter ();

	public:
		void sendLog ( Mat &mat , int count , int frameIndex , int secCount );
};

void *logTransferThreadProc ( void *param );
string encodeBase64 ( unsigned char const* bytes_to_encode , unsigned int in_len );

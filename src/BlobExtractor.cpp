
#include "BlobExtractor.h"
#include "EException.h"
#include "Group.h"

BlobExtractor::BlobExtractor ()
{
	SimpleBlobDetector::Params params;
	params.minThreshold = 30;
	params.maxThreshold = 100;
	params.thresholdStep = 5;
	params.minArea = 10;
	params.minConvexity = 0.3;
	params.minInertiaRatio = 0.01;
	params.maxArea = 8000;
	params.maxConvexity = 10;
	params.filterByColor = false;
	params.filterByCircularity = false;
	m_pBlobDetector = new SimpleBlobDetector ( params );

	m_pBlobDetector->create("SimpleBlob");
}

BlobExtractor::~BlobExtractor ()
{
	delete m_pBlobDetector;
}

void BlobExtractor::processMovingBlob ( Mat frame , double distThreshold )
{
	m_denseArea.clear ();
	m_sparseArea.clear ();

	Mat difference;
	absdiff ( m_prevFrame , m_nextFrame , difference );

	vector<KeyPoint> keyPoints;
	m_pBlobDetector->detect ( difference , keyPoints );

	Group wholeGroup; int i;
	for ( i = 0 ; i < (int)keyPoints.size() ; i++ )
	{
		KeyPoint pt = keyPoints[i];
		wholeGroup.add ( MyPoint ( pt.pt.x , pt.pt.y ) );
	}

	vector<Group> groups;
	wholeGroup.clusterize ( groups , distThreshold );

	for ( i = 0 ; i < (int)groups.size() ; i++ )
	{
		Rect r = groups[i].getRect();
		Rect r2 = groups[i].getRect2();

		try
		{
			r = normalizeRect ( Size(frame.cols,frame.rows) , r );
			r2 = normalizeRect ( Size(frame.cols,frame.rows) , r2 );
		}
		catch ( EException &ex ) { cout << ex.getMessage() << endl; continue; }

		m_denseArea.push_back ( r2 );
		m_sparseArea.push_back ( r );
	}
}

Rect BlobExtractor::normalizeRect ( Size frameSize , Rect srcRect )
{
	if ( srcRect.width > frameSize.width ) { throw EException("WIDTH OVER"); }
	if ( srcRect.height > frameSize.height ) { throw EException("HEIGHT OVER"); }

	int x = srcRect.x; if ( srcRect.x < 0 ) { x = 0; } if ( srcRect.x > frameSize.width ) { throw EException("ERR"); }
	int y = srcRect.y; if ( srcRect.y < 0 ) { y = 0; } if ( srcRect.y > frameSize.height ) { throw EException("ERR"); }
	int w = srcRect.width; if ( x + srcRect.width > frameSize.width ) { w=frameSize.width-srcRect.x; }
	int h = srcRect.height; if ( y + srcRect.height > frameSize.height ) { h=frameSize.height-srcRect.y; }
	Rect ret = Rect ( x , y , w , h );

	if ( ret.width <= 0 ) { throw EException("WIDTH0"); }
	if ( ret.height <= 0 ) { throw EException("HEIGHT0"); }

	return ret;
}

void BlobExtractor::setFrames ( Mat prevFrame , Mat nextFrame )
{
	prevFrame.copyTo(m_prevFrame);
	nextFrame.copyTo(m_nextFrame);

	cvtColor ( m_prevFrame , m_prevFrame , CV_BGR2GRAY );
	cvtColor ( m_nextFrame , m_nextFrame , CV_BGR2GRAY );
}

void BlobExtractor::getDenseArea ( vector<Rect> &area )
{
	area.clear ();
	area.resize ( m_denseArea.size() );
	area.assign ( m_denseArea.begin() , m_denseArea.end () );
}

void BlobExtractor::getSparseArea ( vector<Rect> &area )
{
	area.clear ();
	area.resize ( m_sparseArea.size() );
	area.assign ( m_sparseArea.begin() , m_sparseArea.end () );
}

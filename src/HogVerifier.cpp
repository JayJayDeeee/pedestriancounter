
#include "HogVerifier.h"

HogVerifier::HogVerifier ( int targetCount )
{
	m_maxCount = targetCount;
}

void HogVerifier::addImage ( Mat &mat )
{
	if ( m_maxCount <= (int)m_targetMats.size() ) { return; }
	m_targetMats.push_back ( mat );
}

bool HogVerifier::isValidHog ( Mat &mat )
{
	int i; int positiveCount = 0;

	for ( i = 0 ; i < (int)m_targetMats.size() ; i++ )
	{
		Mat target; m_targetMats[i].copyTo(target);
		if ( getCorrelation ( target , mat ) > 0.4 ) { positiveCount++; }
	}

	if ( positiveCount > m_maxCount/2 ) { return true; }
	return false;
}

double HogVerifier::getCorrelation ( Mat mat1 , Mat mat2 )
{
	double hist = 0.0f;
	Mat mat1hist;
	Mat mat2hist;

	int channels[3] = { 0, 1, 2 }; // 0th & 1st channel
	int histSize[3] = { 60, 60, 60 };
	float rRanges[] = { 0, 256 };
	float gRanges[] = { 0, 256 };
	float bRanges[] = { 0, 256 };
	const float* ranges[] = { rRanges, gRanges, bRanges };

	calcHist ( &mat1 , 1 , channels , Mat() , mat1hist , 2 , histSize , ranges , true , false );
	calcHist ( &mat2 , 1 , channels , Mat() , mat2hist , 2 , histSize , ranges , true , false );
	normalize ( mat1hist , mat1hist , 0 , 1 , NORM_MINMAX , -1 , Mat() );
	normalize ( mat2hist , mat2hist , 0 , 1 , NORM_MINMAX , -1 , Mat() );

	hist = compareHist ( mat1hist , mat2hist , CV_COMP_CORREL );
	return hist;
}


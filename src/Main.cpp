#include "Common.h"
#include "PeopleTag.h"
#include "PeopleTracker.h"
#include "HumanFastFinder.h"
#include "BlobExtractor.h"
#include "EException.h"
#include "LogTransmitter.h"

#define FILENAME "/Users/jayjaydee/Documents/Droptop800600.mp4"
#define HOG_TRAIN_FILENAME "/Users/jayjaydee/Documents/hogcascade_pedestrians.xml"

int main ( void )
{
	BlobExtractor extractor;
	BlobExtractor denseExtractor;
	HumanFastFinder humanFinder;
	PeopleTracker *pTracker = NULL;
	FileStorage fileStorage ( HOG_TRAIN_FILENAME , cv::FileStorage::READ );
	HogVerifier verifier ( 5 );
	LogTransmitter lt;

	//VideoCapture *pVc = new VideoCapture ( FILENAME );
	VideoCapture *pVc = new VideoCapture ( 0 );
	pVc->set( CV_CAP_PROP_FRAME_WIDTH, 640 );
	pVc->set( CV_CAP_PROP_FRAME_HEIGHT, 480 );

	Mat frame; Mat curFrame; Mat prevFrame;
	Rect roiCandidate;
	Rect roiTrack;

	bool isFirst = true;

	vector<Rect> sparseMovingBlob;
	vector<Rect> denseMovingBlob;
	vector<Rect> hog;
	vector<SIMPLETAG> tags;

	int i;
	int frameindex = 0;
	string frameIndexStr = "";

	int prevPeopleCount = 0;
	int peopleCount = 0;
	char buf[80];

	int64 startTime = getTickCount ();
	while ( 1 )
	{
		frameIndexStr = "" + frameindex;
		if ( pVc->grab() == false ) { cout<<"END OF VIDEO"<<endl; break; }
		(*pVc) >> frame; frame.copyTo ( curFrame );
		if ( isFirst == true ) { curFrame.copyTo(prevFrame); isFirst = false; }

		if ( pTracker == NULL )
		{
			roiCandidate = Rect ( 0 , 0 , frame.cols , frame.rows );
			roiTrack = Rect ( 200 , 150 , frame.cols-330 , frame.rows-300 );
			pTracker = new PeopleTracker ( roiCandidate , roiTrack , Size(frame.cols,frame.rows) , &verifier );
		}

		if ( prevFrame.rows == 0 || prevFrame.cols == 0 ||
				curFrame.rows == 0 || curFrame.cols == 0 ) { continue; }

		extractor.setFrames ( prevFrame , curFrame );
		denseExtractor.setFrames ( prevFrame , curFrame );

		extractor.processMovingBlob (curFrame , 150);
		denseExtractor.processMovingBlob ( curFrame , 30 );

		denseExtractor.getDenseArea(denseMovingBlob);
		extractor.getSparseArea(sparseMovingBlob);

		humanFinder.findHumansMultiple ( curFrame , sparseMovingBlob , hog , &fileStorage );

		int a;
		if(frameindex >= 25) {
			a = 3;
		}

		pTracker->update ( frame , hog , denseMovingBlob );
		peopleCount += pTracker->countAndremoveTags();
		pTracker->getResults(tags);

		cout << "index : " << frameindex << endl;
		rectangle ( frame , roiTrack , Scalar(255,255,0) , 3 );

		//for ( i = 0 ; i < (int)sparseMovingBlob.size() ; i++ ) { rectangle ( frame , sparseMovingBlob[i] , Scalar(255,0,0) , 2 ); }
		//for ( i = 0 ; i < (int)denseMovingBlob.size() ; i++ ) { rectangle ( frame , denseMovingBlob[i] , Scalar(255,150,80) , 2 ); }
		for ( i = 0 ; i < (int)hog.size() ; i++ ) { rectangle ( frame , hog[i] , Scalar(255,0,0) , 2 ); }

		string trackStateExpr = "";
		for ( i = 0 ; i < (int)tags.size() ; i++ )
		{
			SIMPLETAG st = tags[i];
			Scalar color;
			if ( st.m_tagType == PeopleTag::TAG_PRIMARY ) { color = Scalar(255,0,0); }
			else { color = Scalar(220,220,250); }

			putText ( frame , st.m_tag , st.m_location , FONT_HERSHEY_COMPLEX_SMALL, 0.8, color, 1, CV_AA);

			Rect rect = Rect ( st.m_location.x-st.m_meanWndSize.width/2 ,
								st.m_location.y-st.m_meanWndSize.height/2 ,
								st.m_meanWndSize.width ,
								st.m_meanWndSize.height );

			switch ( tags[i].m_trackState )
			{
				case PeopleTag::STATE_MEASUREMENT_HOG: trackStateExpr="Measure:1/HoG"; break;
				case PeopleTag::STATE_MEASUREMENT_KALMAN: trackStateExpr="Measure:2/Kalman"; break;
				case PeopleTag::STATE_MEASUREMENT_MOVINGBLOB: trackStateExpr="Measure:3/MovingBlob"; break;
				case PeopleTag::STATE_NONE: trackStateExpr="FAIL"; break;
				case PeopleTag::STATE_PREDICTION: trackStateExpr="Prediction"; break;
			}

			putText ( frame , trackStateExpr ,
					Point(rect.x,rect.y-5) , FONT_HERSHEY_COMPLEX_SMALL, 0.5, color, 1, CV_AA);
			rectangle ( frame , rect , Scalar(0,0,255) , 2 );
		}

		sprintf ( buf , "PEOPLE COUNT : %d" , peopleCount );
		putText ( frame , string(buf) , Point(10,30) , FONT_HERSHEY_COMPLEX_SMALL, 0.8, Scalar(220,220,250) , 1, CV_AA);

		//// FPS calculation!!
		int secCount = (int)((double)(getTickCount()-startTime)/(double)1000000000); if ( secCount == 0 ) { secCount=1; }
		double fps = (double)frameindex/(double)secCount;

		sprintf ( buf , "FPS : %0.2lf" , fps );
		putText ( frame , string(buf) , Point(10,50) , FONT_HERSHEY_COMPLEX_SMALL, 0.8, Scalar(220,220,250) , 1, CV_AA);
		////////

		//// Log Sending ///
		if ( secCount % SEND_INTERVAL == 0 )
		{
			int countInterval = peopleCount - prevPeopleCount;
			lt.sendLog ( frame , countInterval , frameindex , secCount );
			prevPeopleCount = peopleCount;
		}
		//////////

		//Mat doubleframe; frame.copyTo(doubleframe);
		//resize ( doubleframe , doubleframe , Size(doubleframe.cols*2,doubleframe.rows*2));

		imshow ( "NO COUNTRY FOR VISION PROGRAMMERS" , frame );

		curFrame.copyTo ( prevFrame );
		char ch = waitKey (30);

		if ( ch == 'c' ) { break; }
		else if ( ch == 'p' ) { while ( 1 ) { char subCh = waitKey(30); if ( subCh == 'p' ) { break; } } }
		frameindex++;
	}

	if ( pTracker != NULL ) { delete pTracker; }
	delete pVc;
	return 0;
}

/*
 * EException.h
 *
 *  Created on: 2013. 5. 21.
 *      Author: homeqwert
 */

#ifndef EEXCEPTION_H_
#define EEXCEPTION_H_

#include <string>
using namespace std;

class EException {
private:
	string message;

public:
	EException(string _message) { message = _message; }
	virtual ~EException();

	string getMessage() { return message; }
};

#endif /* EEXCEPTION_H_ */


#include "LogTransmitter.h"

LogTransmitter::LogTransmitter ()
{
	curl_global_init ( CURL_GLOBAL_ALL );
	m_prevSecCount = 0;
	m_prevFrameIdx = 0;
}

LogTransmitter::~LogTransmitter ()
{
	curl_global_cleanup();
}

void LogTransmitter::sendLog ( Mat &mat , int count , int frameIndex , int secCount )
{
	if ( m_prevSecCount != secCount )
	{
		m_prevSecCount = secCount;

		if ( m_prevFrameIdx != frameIndex )
		{
			if ( mat.cols == 0 || mat.rows == 0 ) { return; }

			LOG *pLog = new LOG;

			pLog->m_peopleCount = count;
			mat.copyTo(pLog->m_mat);

			THREAD logThreadId;
			pthread_create ( &logThreadId , NULL , logTransferThreadProc , (void *)pLog );
		}

		m_prevFrameIdx = frameIndex;
	}
}

string encodeBase64 ( unsigned char const* bytes_to_encode , unsigned int in_len )
{
	std::string ret;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];

	while (in_len--) {
		char_array_3[i++] = *(bytes_to_encode++);
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for(i = 0; (i <4) ; i++)
				ret += g_base64_chars[char_array_4[i]];
			i = 0;
		}
	}
	if (i)
	{
		for(j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
		char_array_4[3] = char_array_3[2] & 0x3f;

		for (j = 0; (j < i + 1); j++)
			ret += g_base64_chars[char_array_4[j]];

		while((i++ < 3))
			ret += '=';
	}
	return ret;
}

void *logTransferThreadProc ( void *param )
{
	LOG *pLog = (LOG *) param;
	CURL *curl;
	curl = curl_easy_init();

	imwrite ( "test.jpg" , pLog->m_mat );

	ifstream file ( "test.jpg" , ios::in|ios::binary );
	if ( file.is_open() == false ) { curl_easy_cleanup(curl); delete pLog; return NULL; }

	char buf[80];
	sprintf ( buf , "%d" , pLog->m_peopleCount );

	if ( curl != NULL )
	{
		file.seekg(0,ifstream::end);
		int length = file.tellg();
		file.seekg(0,ifstream::beg);

		unsigned char *pBuf = new unsigned char[length+1]; char ch; int idx = 0;
		while ( !file.eof() ) { file.get(ch); pBuf[idx]=ch; idx++; }
		file.close();

		string base64expr = encodeBase64 ( pBuf , length );
		delete pBuf;

		string totalExpr =
				"id=" + string(SERVER_ID) +
				"&count=" + string(buf) + "&data=" + base64expr;

		cout << "TOTAL LENGTH : " << totalExpr.length() << endl;

		curl_easy_setopt(curl, CURLOPT_URL, SERVER_ADDR );
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, totalExpr.c_str());
		curl_easy_perform(curl);

		curl_easy_cleanup(curl);
	}

	delete pLog;
	return NULL;
}

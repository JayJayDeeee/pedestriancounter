#include "HumanFastFinder.h"

void HumanFastFinder::findHumans ( Mat mat , vector<Rect> &humanAreas , CascadeClassifier *pClassifier )
{
	pClassifier->detectMultiScale( mat , humanAreas );
}

void HumanFastFinder::findHumansSingle ( Mat frame , vector<Rect> pieces , vector<Rect> &humanAreas , CascadeClassifier *pClassifier )
{
	vector<Rect> result;
	Mat mat; frame.copyTo(mat);

	int i; int j;
	for ( i = 0 ; i < (int)pieces.size() ; i++ )
	{
		Mat cropped; Rect r = pieces[i];
		int x = r.x; if ( r.x > mat.cols ) { x = mat.cols; } if ( r.x < 0 ) { x = 0; }
		int y = r.y; if ( r.y > mat.rows ) { y = mat.rows; } if ( r.y < 0 ) { y = 0; }
		int w = r.width; if ( x + r.width > mat.cols ) { w = mat.cols - x; }
		int h = r.height; if ( y + r.height > mat.rows ) { h = mat.rows - y; }

		r = Rect ( x , y , w , h );
		Mat(mat,r).copyTo(cropped);

		vector<Rect> humanArea;
		pClassifier->detectMultiScale(cropped,humanArea);

		for ( j = 0 ; j < (int)humanArea.size() ; j++ )
		{
			Rect outputRect = humanArea[j];
			result.push_back (
					Rect(outputRect.x + r.x , outputRect.y + r.y ,
							outputRect.width , outputRect.height) );
		}

	}

	humanAreas.clear ();
	humanAreas.resize ( result.size() );
	humanAreas.assign ( result.begin() , result.end () );
}

void HumanFastFinder::findHumansMultiple ( Mat frame , vector<Rect> pieces , vector<Rect> &humanAreas , FileStorage *pFs )
{
	THREAD *pThread = new THREAD[pieces.size()];
	HOGRESULT **pResult = new HOGRESULT *[pieces.size()];
	vector<Rect> duplicateResult;
	vector<Rect> finalResult;

	int i;
	for ( i = 0 ; i < (int)pieces.size() ; i++ )
	{
		Rect r = pieces[i];

		int x = r.x; if ( r.x > frame.cols ) { x = frame.cols; } if ( r.x < 0 ) { x = 0; }
		int y = r.y; if ( r.y > frame.rows ) { y = frame.rows; } if ( r.y < 0 ) { y = 0; }
		int w = r.width; if ( x + r.width > frame.cols ) { w = frame.cols - x; }
		int h = r.height; if ( y + r.height > frame.rows ) { h = frame.rows - y; }
		r = Rect ( x , y , w , h );

		HOGINFO *pInfo = new HOGINFO;
		frame.copyTo(pInfo->m_frame);
		pInfo->m_rect = r;
		pInfo->m_pFs = pFs;

		pthread_create ( &pThread[i] , NULL , hogFindProc , (void *)pInfo );
	}

	for ( i = 0 ; i < (int)pieces.size() ; i++ ) { pthread_join ( pThread[i] , (void **)&(pResult[i]) ); }
	for ( i = 0 ; i < (int)pieces.size() ; i++ )
	{
		int j; HOGRESULT *pRes = pResult[i];
		for ( j = 0 ; j < (int)pRes->m_result.size() ; j++ ) { duplicateResult.push_back ( pRes->m_result[j] ); }
		delete pRes;
	}
	delete pResult;

	int duplicateResultSize = (int) duplicateResult.size();
	bool *isChecked = new bool[duplicateResultSize];
	for( i = 0; i < duplicateResultSize; i++) {
		isChecked[i] = false;
	}

	for( i = 0; i < duplicateResultSize; i++) {
		if(isChecked[i] == true) { continue; }

		for( int j = 1; j < duplicateResultSize; j++) {
			if(isChecked[j] == true) { continue; }

			int duplicatePoint1X = duplicateResult[i].x - duplicateResult[i].width/2;
			int duplicatePoint1Y = duplicateResult[i].y - duplicateResult[i].height/2;

			int duplicatePoint2X = duplicateResult[j].x - duplicateResult[j].width/2;
			int duplicatePoint2Y = duplicateResult[j].y - duplicateResult[j].height/2;

			int duplicateRange1Width = duplicatePoint1X + (int) (duplicateResult[i].width * 0.7);
			int duplicateRange1Height = duplicatePoint1Y + (int) (duplicateResult[i].height * 0.7);

			int duplicateRange2Width = duplicatePoint2X + (int) (duplicateResult[j].width * 0.7);
			int duplicateRange2Height = duplicatePoint2Y + (int) (duplicateResult[j].height * 0.7);

			if( ( (duplicatePoint2X >= duplicatePoint1X && duplicatePoint2X <= duplicateRange1Width) &&
					(duplicatePoint2Y >= duplicatePoint1Y && duplicatePoint2Y <= duplicateRange1Height) ) ||
					( (duplicatePoint1X >= duplicatePoint2X && duplicatePoint1X <= duplicateRange2Width) &&
							(duplicatePoint1Y >= duplicatePoint2Y && duplicatePoint1Y <= duplicateRange2Height) ) ) {
				isChecked[j] = true;
			}

		}
		finalResult.push_back(duplicateResult[i]);
	}

	humanAreas.clear ();
	humanAreas.resize ( finalResult.size() );
	humanAreas.assign ( finalResult.begin() , finalResult.end () );
}

void *hogFindProc ( void *param )
{
	HOGINFO *pInfo = (HOGINFO *)param;
	HOGRESULT *pResult = new HOGRESULT;

	CascadeClassifier hog;
	hog.read ( pInfo->m_pFs->getFirstTopLevelNode() );

	Mat cropped; Mat(pInfo->m_frame,pInfo->m_rect).copyTo(cropped);
	vector<Rect> result; vector<Rect> temp;

	hog.detectMultiScale( cropped , result );
	int i;
	for ( i = 0 ; i < (int)result.size() ; i++ )
	{
		temp.push_back (
				 Rect(pInfo->m_rect.x + result[i].x ,
						pInfo->m_rect.y + result[i].y ,
						result[i].width ,
						result[i].height) );
	}

	pResult->m_result.resize ( temp.size() );
	pResult->m_result.assign ( temp.begin() , temp.end() );

	delete pInfo;
	return (void *)pResult;
}

#include "Group.h"
#include "MyPoint.h"

Rect Group::getRect ( void )
{
	Rect ret; int i;
	int minX = 200000; int minY = 200000;
	int maxX = 0; int maxY = 0;
	for ( i = 0 ; i < mList.size() ; i++ )
	{
		MyPoint pt = mList[i];
		if ( minX > pt.getX() ) { minX = pt.getX (); }
		if ( minY > pt.getY() ) { minY = pt.getY (); }
		if ( maxX < pt.getX() ) { maxX = pt.getX (); }
		if ( maxY < pt.getY() ) { maxY = pt.getY (); }
	}
	ret.x = minX - 50; ret.y = minY-50;
	ret.width = maxX - minX + 80;
	ret.height = maxY - minY + 80;
	return ret;
}

Rect Group::getRect2 ( void )
{
	vector<Point> points;
	int i; for ( i = 0 ; i < (int)mList.size() ; i++ )
	{ points.push_back ( Point ( mList[i].getX() , mList[i].getY() ) ); }
	Rect ret = boundingRect ( points );
	return ret;
}

void Group::clusterize ( vector<Group> &groupList , double threshold )
{
	/// 인접행렬 초기화
	int **mat = new int *[mList.size()]; int i; int j;
	vector<int> vertex;
	vector<int> visited;
	vector<int> groupped;

	for ( i = 0 ; i < mList.size () ; i++ )
	{
		vertex.push_back ( i );
		visited.push_back ( 0 );
		groupped.push_back ( 0 );

		mat[i] = new int [mList.size()];
		for ( j = 0 ; j < mList.size() ; j++ ) { mat[i][j] = 0; }
	}

	// Create Edge using threshold
	for ( i = 0 ; i < mList.size () ; i++ )
	{
		MyPoint ptrI = mList[i];
		for ( j = 0 ; j < mList.size() ; j++ )
		{
			MyPoint ptrJ = mList[j];
			if ( ptrI.getDistance ( ptrJ ) < threshold )
			{ if ( mat[j][i] != 1 ) { mat[i][j] = 1; } }
		}
	}

	// traversing each graph components
	for ( i = 0 ; i < mList.size () ; i++ )
	{
		if ( groupped[i] == 1 ) { continue; }
		Group newgroup;
		stack<int> stk; stk.push ( i );
		while ( !stk.empty() )
		{
			int value = stk.top(); stk.pop ();
			visited[value] = 1; groupped[value] = 1;
			newgroup.add ( mList[value] );
			for ( j = 0 ; j < mList.size() ; j++ )
			{
				if ( visited[j] != 1 && mat[value][j] == 1 )  { stk.push ( j ); }
			}
		}
		groupList.push_back ( newgroup );
	}

	// 인접행렬 메모리 할당해제
	for ( i = 0 ; i < mList.size () ; i++ ) { delete mat[i]; }
	delete mat;
}



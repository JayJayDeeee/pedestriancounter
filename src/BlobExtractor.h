#pragma once

#include "Common.h"
#include <opencv2/features2d/features2d.hpp>

class BlobExtractor
{
private:
	SimpleBlobDetector *m_pBlobDetector;
	Mat m_prevFrame;
	Mat m_nextFrame;
	vector<Rect> m_denseArea;
	vector<Rect> m_sparseArea;

public:
	BlobExtractor ();
	~BlobExtractor ();

public:
	void processMovingBlob ( Mat frame , double distThreshold );
	void setFrames ( Mat prevFrame , Mat nextFrame );
	void getDenseArea ( vector<Rect> &area );
	void getSparseArea ( vector<Rect> &area );

private:
	Rect normalizeRect ( Size frameSize , Rect srcRect );
};

#pragma once

class MyPoint
{
private:
    int mX; int mY;

public:
    MyPoint () { mX = 0; mY = 0; }
    MyPoint ( int x , int y ) { mX = x; mY = y; }
    void setPoint ( int x , int y ) { mX = x; mY = y;  }
    int getX () { return mX; }
    int getY () { return mY; }
    double getDistance ( MyPoint p ) { return (double)sqrt((mX-p.mX)*(mX-p.mX)+(mY-p.mY)*(mY-p.mY)); }
};

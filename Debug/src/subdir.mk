################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/BlobExtractor.cpp \
../src/EException.cpp \
../src/Group.cpp \
../src/HogVerifier.cpp \
../src/HumanFastFinder.cpp \
../src/LogTransmitter.cpp \
../src/Main.cpp \
../src/PeopleTag.cpp \
../src/PeopleTracker.cpp 

OBJS += \
./src/BlobExtractor.o \
./src/EException.o \
./src/Group.o \
./src/HogVerifier.o \
./src/HumanFastFinder.o \
./src/LogTransmitter.o \
./src/Main.o \
./src/PeopleTag.o \
./src/PeopleTracker.o 

CPP_DEPS += \
./src/BlobExtractor.d \
./src/EException.d \
./src/Group.d \
./src/HogVerifier.d \
./src/HumanFastFinder.d \
./src/LogTransmitter.d \
./src/Main.d \
./src/PeopleTag.d \
./src/PeopleTracker.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/local/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


